# README #

### What is this repository for? ###

This is a plugin for X-Plane, aiming at retrive information about datarefs (if they exist, are writable and their types).

### How do I use it ? ###

Create a file datarefList.txt in X-Plane's root folder (next to X-Plane executable).
This file must contain 1 dataref per line.

When launching X-Plane, the plugin will read this file and check every dataref mentioned.
A new file, datarefOutput.csv, will be created next to the .txt you provided.

This .csv contains the datarefs found in your .txt, if they have been found in 
X-Plane (typically custom datarefs can require a specific plugin to create them), 
if they are writable and their type (as definded in X-Plane's SDK).

### How do I get set up? ###

This is a basic plugin for X-Plane using Qt Creator as IDE.
Please check the X-Plane's SDK path in the .pro file.

### Who do I talk to? ###

Made by Camille Bachmann