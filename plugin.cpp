/*
Author: Camille Bachmann
Version: 1.0
Version date: 20/03/2020
*/

#include <string>
#include <vector>
#include <fstream>

#include "XPLMPlugin.h"
#include "XPLMUtilities.h"
#include "XPLMDataAccess.h"
#include "XPLMProcessing.h"

#include "dllmainwindows.h"

using namespace std;

static char systemPath[512];
static string datarefListFile;
static string outputFile;

static vector<string> datarefs;
static vector<bool> found;
static vector<int> writable;
static vector<int> types;

float flightLoop(float sinceLastCall, float sinceLastLoop, int inCounter, void* inRefcon);
static XPLMCreateFlightLoop_t flightLoopStruct{ sizeof(XPLMCreateFlightLoop_t), xplm_FlightLoop_Phase_AfterFlightModel, flightLoop, nullptr };
static XPLMFlightLoopID flightLoopID;

PLUGIN_API int XPluginStart(char * outName, char * outSig, char * outDesc)
{
    const std::string pluginName = "DatarefInfoGrab";
    const std::string pluginSignature = "SquirrelDev.DatarefInfoGrab";
    const std::string pluginDescription = "Plugin to read datarefs informations by Camille Bachmann, V1.0";
#if IBM
    strcpy_s(outName, 256, pluginName.c_str());
    strcpy_s(outSig, 256, pluginSignature.c_str());
    strcpy_s(outDesc, 256, pluginDescription.c_str());
#else
    strcpy(outName, pluginName.c_str());
    strcpy(outSig,  pluginSignature.c_str());
    strcpy(outDesc, pluginDescription.c_str());
#endif
    XPLMGetSystemPath(systemPath);

    datarefListFile = string(systemPath) + "datarefList.txt";
    outputFile = string(systemPath) + "datarefOutput.csv";

    ifstream readStream(datarefListFile);
    if(readStream)
    {
        string line;
        while(getline(readStream, line))
        {
            if(line.size() > 0)
            {
                datarefs.push_back(line);
                found.push_back(false);
                writable.push_back(0);
                types.push_back(0);
            }
        }
    }

    flightLoopID = XPLMCreateFlightLoop(&flightLoopStruct);
    XPLMScheduleFlightLoop(flightLoopID, 10.f, 1);

    return 1;
}

PLUGIN_API void XPluginReceiveMessage(XPLMPluginID, int, void *)
{
}

PLUGIN_API void  XPluginStop(void)
{
    XPLMDestroyFlightLoop(flightLoopID);
}

PLUGIN_API void XPluginDisable(void)
{
}

PLUGIN_API int XPluginEnable(void)
{
    datarefs.clear();
    found.clear();
    writable.clear();
    types.clear();

    ifstream readStream(datarefListFile);
    if(readStream)
    {
        string line;
        while(getline(readStream, line))
        {
            if(line.size() > 0)
            {
                datarefs.push_back(line);
                found.push_back(false);
                writable.push_back(0);
                types.push_back(0);
            }
        }
    }

    XPLMScheduleFlightLoop(flightLoopID, 5.f, 1);

    return 1;
}

float flightLoop(float sinceLastCall, float sinceLastLoop, int inCounter, void* inRefcon)
{
    (void) sinceLastCall;
    (void) sinceLastLoop;
    (void) inCounter;
    (void) inRefcon;

    for(size_t i=0;i<datarefs.size();i++)
    {
        XPLMDataRef dataref = XPLMFindDataRef(datarefs.at(i).c_str());
        if(dataref)
        {
            found.at(i) = true;
            writable.at(i) = XPLMCanWriteDataRef(dataref);
            types.at(i) = XPLMGetDataRefTypes(dataref);
        }
        else
        {
            found.at(i) = false;
        }
    }

    ofstream outputStream(outputFile);
    if(outputStream)
    {
        outputStream << "Dataref;Found;Writable;Type(s)" << endl;
        for(size_t i=0;i<datarefs.size();i++)
        {
            outputStream << "\"" << datarefs.at(i) << "\";" << found.at(i) << ";" << writable.at(i) << ";" << types.at(i) << endl;
        }
    }

    XPLMSpeakString("Finished testing datarefs!");

    return 0.f;
}
