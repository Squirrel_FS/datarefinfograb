# Shared library without any Qt functionality
TEMPLATE = lib
QT -= gui core

CONFIG += warn_on plugin c++11
CONFIG -= thread qt rtti debug_and_release_target

VERSION = 1.0.0

DEFINES += XPLM200
DEFINES += XPLM210
xplane11 {
    DEFINES += XPLM300
    DEFINES += XPLM301
}

debug {
    DEFINES += DEBUG
} else {
    CONFIG += release
}

win32 {
    xplane11 {
        INCLUDEPATH += D:/X-Plane/SDK301/CHeaders/XPLM
        INCLUDEPATH += D:/X-Plane/SDK301/CHeaders/Wrappers
        INCLUDEPATH += D:/X-Plane/SDK301/CHeaders/Widgets
        LIBS += -L"D:/X-Plane/SDK301/Libraries/Win"
    } else {
        INCLUDEPATH += D:/X-Plane/SDK212/CHeaders/XPLM
        INCLUDEPATH += D:/X-Plane/SDK212/CHeaders/Wrappers
        INCLUDEPATH += D:/X-Plane/SDK212/CHeaders/Widgets
        LIBS += -L"D:/X-Plane/SDK212/Libraries/Win"
        TARGET = win
    }
    build32 {
        LIBS += -lXPLM -lXPWidgets
    } else {
        LIBS += -lXPLM_64 -lXPWidgets_64
    }
    DEFINES += APL=0 IBM=1 LIN=0
    TARGET_EXT = .xpl
}

unix:!macx {
    xplane11 {
        INCLUDEPATH += /mnt/d/X-Plane/SDK301/CHeaders/XPLM
        INCLUDEPATH += /mnt/d/X-Plane/SDK301/CHeaders/Wrappers
        INCLUDEPATH += /mnt/d/X-Plane/SDK301/CHeaders/Widgets
    } else {
        INCLUDEPATH += /mnt/d/X-Plane/SDK212/CHeaders/XPLM
        INCLUDEPATH += /mnt/d/X-Plane/SDK212/CHeaders/Wrappers
        INCLUDEPATH += /mnt/d/X-Plane/SDK212/CHeaders/Widgets
        TARGET = lin
    }
    CONFIG += no_plugin_name_prefix
    DEFINES += APL=0 IBM=0 LIN=1
    QMAKE_EXTENSION_SHLIB = xpl
    QMAKE_CXXFLAGS += -fvisibility=hidden -shared -rdynamic -nodefaultlibs -undefined_warning
}

macx {
    xplane11 {
    } else {
        TARGET = mac
    }
    TARGET_EXT = xpl
    DEFINES += APL=1 IBM=0 LIN=0
    QMAKE_LFLAGS += -undefined suppress
    CONFIG += x64 ppc
}

SOURCES += \
    dllmainwindows.cpp \
    plugin.cpp

HEADERS += \
    dllmainwindows.h
